

(function() {
	
	tinymce.PluginManager.requireLangPack('unipd4');

	tinymce.create('tinymce.plugins.Unipd4Plugin', {
		
		init : function(ed, url) {
			this.editor = ed;
		
			ed.addCommand('mceUnipd4', function() {
				ed.windowManager.open({
					file : url + '/dialog4.htm',
					width : 500,
					height : 460,
					inline : 1
				}, {
					plugin_url : url 
				});
			});
			ed.addCommand('UnipdResize',function(ui,v) {
    			ed.windowManager.params.mce_height = v.height + 16; 
			});

			
			ed.addButton('unipd4', {title : 'unipd4.desc', cmd : 'mceUnipd4', image : url + '/img/muvereiconmovio.png' });
		},
		
		getInfo : function() {
			return {
				longname : 'Unipd4 plugin',
				author : 'Federico Rizzato',
				authorurl : '',
				infourl : '',
				version : "1.0"
			};
		}
	});

	
	tinymce.PluginManager.add('unipd4', tinymce.plugins.Unipd4Plugin);
})(tinymce);
