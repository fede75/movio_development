
(function() {
	
	tinymce.PluginManager.requireLangPack('unipd4');

	tinymce.create('tinymce.plugins.Unipd4Plugin', {
		
		init : function(ed, url) {
			
			ed.addCommand('mceUnipd4', function() {
				ed.windowManager.open({
					file : url + '/dialog4.html',
					width : 320 + parseInt(ed.getLang('unipd4.delta_width', 0)),
					height : 120 + parseInt(ed.getLang('unipd4.delta_height', 0)),
					inline : 1
				}, {
					plugin_url : url, 
					some_custom_arg : 'custom arg' 
				});
			});

			
			ed.addButton('unipd4', {
				title : 'unipd4.desc',
				cmd : 'mceUnipd4',
				image : url + '/img/muvereiconmovio.png'
			});

			
			ed.onNodeChange.add(function(ed, cm, n) {
				cm.setActive('unipd4', n.nodeName == 'IMG');
			});
		},

		
		createControl : function(n, cm) {
			return null;
		},

		
		getInfo : function() {
			return {
				longname : 'Unipd4 plugin',
				author : 'Some author',
				authorurl : 'http://tinymce.moxiecode.com',
				infourl : 'http://wiki.moxiecode.com/index.php/TinyMCE:Plugins/unipd4',
				version : "1.0"
			};
		}
	});

	
	tinymce.PluginManager.add('unipd4', tinymce.plugins.Unipd3Plugin);
})();