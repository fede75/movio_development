
(function() {
	
	tinymce.PluginManager.requireLangPack('unipd3');

	tinymce.create('tinymce.plugins.Unipd3Plugin', {
		
		init : function(ed, url) {
			
			ed.addCommand('mceUnipd3', function() {
				ed.windowManager.open({
					file : url + '/dialog3.html',
					width : 320 + parseInt(ed.getLang('unipd3.delta_width', 0)),
					height : 120 + parseInt(ed.getLang('unipd3.delta_height', 0)),
					inline : 1
				}, {
					plugin_url : url, 
					some_custom_arg : 'custom arg' 
				});
			});

			
			ed.addButton('unipd3', {
				title : 'unipd3.desc',
				cmd : 'mceUnipd3',
				image : url + '/img/ricerca.png'
			});

			
			ed.onNodeChange.add(function(ed, cm, n) {
				cm.setActive('unipd3', n.nodeName == 'IMG');
			});
		},

		
		createControl : function(n, cm) {
			return null;
		},

		
		getInfo : function() {
			return {
				longname : 'Unipd3 plugin',
				author : 'Some author',
				authorurl : 'http://tinymce.moxiecode.com',
				infourl : 'http://wiki.moxiecode.com/index.php/TinyMCE:Plugins/unipd3',
				version : "1.0"
			};
		}
	});

	
	tinymce.PluginManager.add('unipd3', tinymce.plugins.Unipd3Plugin);
})();