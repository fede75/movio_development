

(function() {
	
	tinymce.PluginManager.requireLangPack('unipd2');

	tinymce.create('tinymce.plugins.Unipd2Plugin', {
		
		init : function(ed, url) {
			this.editor = ed;
		
			ed.addCommand('mceUnipd2', function() {
				ed.windowManager.open({
					file : url + '/dialog2.htm',
					width : 500,
					height : 460,
					inline : 1
				}, {
					plugin_url : url 
				});
			});
			ed.addCommand('UnipdResize',function(ui,v) {
    			ed.windowManager.params.mce_height = v.height + 16; 
			});

			
			ed.addButton('unipd2', {title : 'unipd2.desc', cmd : 'mceUnipd2', image : url + '/img/phaidra.png' });
		},
		
		getInfo : function() {
			return {
				longname : 'Unipd2 plugin',
				author : 'Federico Rizzato',
				authorurl : '',
				infourl : '',
				version : "1.0"
			};
		}
	});

	
	tinymce.PluginManager.add('unipd2', tinymce.plugins.Unipd2Plugin);
})(tinymce);
