function HideElement(el){
	document.getElementById(el).style.display='none';
	document.getElementById(el).style.visibility='hidden';
}

function ShowElement(el){
	document.getElementById(el).style.display='inherit';
	document.getElementById(el).style.visibility='visible';
}

function LinkifyURLs(myText){
	// Linkify URLs (since Phaidra 2.9 URLs are not converted into links any more, here's a temporary workaround)

	// Remove Phaidra 2.8 {link}{/link} tags
	var replacePattern0 = /({link}|{\/link})/gi;
	myText = myText.replace(replacePattern0, '');
	
	// Fix links where angle brackets had been converted into HTML entities &lt; &gt; by Phaidra
	var replacePattern1 = /&lt;a(.*?)href="(.*?)"(.*?)&gt;(.*?)&lt;\/a&gt;/gi;
	myText = myText.replace(replacePattern1, '<a$1href="$2"$3>$4</a>');
	
	// Replace text for plain URLs (http://, https://, ftp://). Just avoid the replacement when URLs are surrounded by quotes or doublequotes, since already working.
	var replacePattern2 = /((https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;\u0100-\u017F\u00A0-\u00FF\u0180-\u024F()]*[-A-Z0-9+&@#\/%=~_|\u0100-\u017F\u00A0-\u00FF\u0180-\u024F()])/gi;
	myText = myText.replace(replacePattern2, '<a href="$1">$1</a>');

	var replacePattern2 = /(\s(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gi;
	myText = myText.replace(replacePattern2, '<a href="$1">$1</a>');

	// While we are at it, let's remove unwanted line breaks, and fix common misspellings
	myText = myText.replace(/^\s+\r?\n|\r|\t/g,'').replace(/E'/g,'�').replace(/<br \/>\n/,'');
		
	return myText;
}

function CheckPhaidraId(field){
    var form_valid = (document.getElementById(field).value !== '');
    if(!form_valid){
        alert('Devi specificare l\'identificativo');
        return false;
    }else{
    	UnipdDialog.insertPhaidra();
    }
}