
(function() {
	
	tinymce.PluginManager.requireLangPack('unipd5');

	tinymce.create('tinymce.plugins.Unipd5Plugin', {
		
		init : function(ed, url) {
			
			ed.addCommand('mceUnipd5', function() {
				ed.windowManager.open({
					file : url + '/dialog5.html',
					width : 320 + parseInt(ed.getLang('unipd5.delta_width', 0)),
					height : 120 + parseInt(ed.getLang('unipd5.delta_height', 0)),
					inline : 1
				}, {
					plugin_url : url, 
					some_custom_arg : 'custom arg' 
				});
			});

			
			ed.addButton('unipd5', {
				title : 'unipd5.desc',
				cmd : 'mceUnipd5',
				image : url + '/img/phaidramuseo.png'
			});

			
			ed.onNodeChange.add(function(ed, cm, n) {
				cm.setActive('unipd5', n.nodeName == 'IMG');
			});
		},

		
		createControl : function(n, cm) {
			return null;
		},

		
		getInfo : function() {
			return {
				longname : 'Unipd5 plugin',
				author : 'Some author',
				authorurl : 'http://tinymce.moxiecode.com',
				infourl : 'http://wiki.moxiecode.com/index.php/TinyMCE:Plugins/unipd5',
				version : "1.0"
			};
		}
	});

	
	tinymce.PluginManager.add('unipd5', tinymce.plugins.Unipd5Plugin);
})();