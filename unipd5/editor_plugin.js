

(function() {
	
	tinymce.PluginManager.requireLangPack('unipd5');

	tinymce.create('tinymce.plugins.Unipd5Plugin', {
		
		init : function(ed, url) {
			this.editor = ed;
		
			ed.addCommand('mceUnipd5', function() {
				ed.windowManager.open({
					file : url + '/dialog5.htm',
					width : 500,
					height : 460,
					inline : 1
				}, {
					plugin_url : url 
				});
			});
			ed.addCommand('UnipdResize',function(ui,v) {
    			ed.windowManager.params.mce_height = v.height + 16; 
			});

			
			ed.addButton('unipd5', {title : 'unipd5.desc', cmd : 'mceUnipd5', image : url + '/img/phaidramuseo.png' });
		},
		
		getInfo : function() {
			return {
				longname : 'Unipd5 plugin',
				author : 'Federico Rizzato',
				authorurl : '',
				infourl : '',
				version : "1.0"
			};
		}
	});

	
	tinymce.PluginManager.add('unipd5', tinymce.plugins.Unipd5Plugin);
})(tinymce);
